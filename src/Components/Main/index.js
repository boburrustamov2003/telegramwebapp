import { useState, useEffect } from "react";
import Card from "./Card/Card";
import Cart from "./Cart/Cart";

import { useNavigate } from "react-router-dom";

const { getData } = require("../../db/db");
const foods = getData();



const tele = window.Telegram.WebApp;
function Main({ onGetItem }) {
    const [cartItems, setCartItems] = useState([]);


    const navigate = useNavigate();
    const [username, setUsername] = useState();



    useEffect(() => {
        tele.ready();


        // let initData = window.Telegram.WebApp.initData || '';
        if (window.Telegram.WebApp.initDataUnsafe.user) {
            let initDataUnsafe = window.Telegram.WebApp.initDataUnsafe || {};
            setUsername(initDataUnsafe.user.username);
        }


        // window.Telegram.WebApp.BackButton.isVisible = true;
        window.Telegram?.WebApp.MainButton.onClick(() => {
            navigate('/order')
        });
        onGetItem(cartItems)


        const localCartItems = window.localStorage.getItem("cartItems");
        if (localCartItems) {
            setCartItems(JSON.parse(localCartItems));
        }
    }, [navigate, cartItems, onGetItem]);


    useEffect(() => {
        const getItem = JSON.parse(localStorage.getItem('item'))
        if (getItem) {
            setCartItems(getItem)
        }
    }, [])



    const onAdd = (food) => {
        const exist = cartItems.find((x) => x.id === food.id);
        if (exist) {
            setCartItems(
                cartItems.map((x) =>
                    x.id === food.id ? { ...exist, quantity: exist.quantity + 1 } : x
                )
            );
            console.log(true)
        } else {
            setCartItems([...cartItems, { ...food, quantity: 1 }]);
        }

    };



    const onRemove = (food) => {
        const exist = cartItems.find((x) => x.id === food.id);
        if (exist.quantity === 1) {
            setCartItems(cartItems.filter((x) => x.id !== food.id));
        } else {
            setCartItems(
                cartItems.map((x) =>
                    x.id === food.id ? { ...exist, quantity: exist.quantity - 1 } : x
                )
            );
        }
    };


    if (cartItems.length > 0) {
        tele.MainButton.text = "VIEW ORDER";
        tele.MainButton.show();
    } else {
        tele.MainButton.hide();
    }


    // const onCheckout = () => {
    //     navigate('/order')
    // };


    return (
        <>
            <div>
                <h1 className="heading">Order Food  </h1>{/* ({username}) */}
                {/* <Cart cartItems={cartItems} onCheckout={onCheckout} /> */}
                <div className="cards__container">
                    {
                        foods.map((food) => {
                            return (
                                <Card food={food} key={food.id} onAdd={onAdd} onRemove={onRemove} />
                            );
                        })
                    }
                </div>
            </div>
        </>
    );
}

export default Main;
