import React, { useState, useEffect } from 'react'
import './index.css'
import { Link } from "react-router-dom";
import MainButton from '@twa-dev/mainbutton';
import Success from './success';

import { useNavigate } from "react-router-dom";

const tele = window.Telegram.WebApp;

function Order({ items }) {
    const [order, setOrder] = useState({})
    const [price, setPrice] = useState()
    const [username, setUsername] = useState()

    const navigate = useNavigate();

    const totalPrice = items.reduce((a, c) => a + c.price * c.quantity, 0);
    const totalQuantity = items.reduce((a, c) => a + c.quantity, 0);




    useEffect(() => {
        tele.ready();
        window.Telegram.WebApp.MainButton.text = `Order Now $${totalPrice.toFixed(2)}`;
        window.Telegram.WebApp.MainButton.show();



        setOrder({
            username: `${username}`,
            quantity: `${totalQuantity}`,
            price: `${totalPrice}`,
        })



        if (window.Telegram.WebApp.initDataUnsafe.user) {
            let initDataUnsafe = window.Telegram.WebApp.initDataUnsafe || {};
            setUsername(initDataUnsafe.user.username);
            // window.Telegram.WebApp.BackButton.
            window.Telegram.WebApp.BackButton.onClick(() => {
                navigate('/')
            });

            // tele.offEvent('backButtonClicked', navigate('/'))
        }



    }, [navigate, totalPrice, totalQuantity, username])



    function handleClick() {
        if (order) {
            fetch('https://telebackend.somee.com/order', {
                method: 'POST',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify(order)

            }).then(r => r.json()).then(res => {
                if (res) {
                    console.log(order)
                }
            })
            setOrder({})
        }
        alert('Your order is Success')
        navigate('/')
    }



    return (
        <>
            {/* <Success /> */}
            <div className='order_Section'>
                <div className="order_top">
                    <h2 className='order_top_title'>Your Order</h2>
                    <Link to='/' className='order_edit'>Edit</Link>
                </div>
                {
                    items.map(item => {
                        const { Image, id, price, quantity, title, category } = item
                        const allPrice = price * quantity


                        return (
                            <div key={id} className="orders_menu">
                                <div className="orders_menu-left">
                                    <img className='order_menu_img' src={Image} alt={title} />
                                    <div className="order_menu_title">
                                        <h3 className='order_menu_title-h3'>{title} <span className='order_quantity'>{quantity}x</span> </h3>
                                        <h4 className='order_menu_category'>{category}</h4>
                                    </div>
                                </div>
                                <h4>{allPrice.toFixed(2)}</h4>
                            </div>
                        )
                    })
                }
                {/* <button onClick={handleClick}>order</button> */}
                {/* <p>{JSON.stringify(order)}</p> */}
            </div>
            <MainButton onClick={() => handleClick()} />
        </>
    )
}

export default Order