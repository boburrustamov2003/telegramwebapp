import React, { useState } from 'react'
import Main from './Main'
import Order from './Order'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function Component() {
    const [items, setItem] = useState([])

    function onGetItem(item) {
        setItem(item)
    }
    console.log(items)

    return (
        <div>
            <Router>
                <Routes>
                    <Route path='/' element={<Main onGetItem={onGetItem} />} />
                    <Route path='/order' element={<Order items={items} />} />
                </Routes>
            </Router>
        </div>
    )
}

export default Component